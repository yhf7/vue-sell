# 高仿饿了么商家页面

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

# 外卖 app 商家页面分析

## 需求分析

- 头部

  - 商家描述
  - 浮层

- 内容
  - 商品（保持联动效果）
    - 商品分类
    - 实际商品列表
      - 商品介绍
      - 图片自适应
      - 客户评论列表
    - 购物车
      - 购物车列表
      - 最低购买价
  - 评价
    - 客户评论
  - 商家详情
    - 商家的评星等

## 项目需求素材

- 设计图
- 字体图标


## 项目目录设计
- src
  - common(公共资源)
    - js
    - fonts
    - stylus
  - components(vue页面)
  
    
## mock 后端数据
- data.json
  - seller 商家相关的数据
  - goods 商品/分类/商品名称价格等
  - ratings 商品评论/用户名/时间