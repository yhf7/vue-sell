import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  linkActiveClass: 'active',
  routes: [
    {
      path: '/',
      redirect: '/goods'
    },
    // {
    //   path: '/about',
    //   name: 'about',
    //   // route level code-splitting
    //   // this generates a separate chunk (about.[hash].js) for this route
    //   // which is lazy-loaded when the route is visited.
    //   component: () => import(/* webpackChunkName: 'about' */ './views/About.vue')
    // }
    {
      path: '/goods',
      name: 'goods',
      component: () => import('./components/goods/goods.vue')
    },
    {
      path: '/header',
      name: 'header',
      component: () => import('./components/header/header.vue')
    },
    {
      path: '/seller',
      name: 'seller',
      component: () => import('./components/seller/seller.vue')
    }, {
      path: '/ratings',
      name: 'ratings',
      component: () => import('./components/ratings/ratings.vue')
    }
  ]
})
