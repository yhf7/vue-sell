const appData = require('./mock/data.json')
const seller = appData.seller
const goods = appData.goods
const ratings = appData.ratings

module.exports = {
  // baseUrl: '/',
  devServer: {
    // proxy: {
    //   '/api': {
    //     target: 'http://localhost:5000',
    //     changeOrigin: true,
    //     pathRewrite: {
    //       '^/api': ''
    //     }
    //   }
    // }
    port: 8080,
    before (app) {
      app.get('/api/seller', (req, res) => {
        // res.json(mockdata)
        res.json({
          errno: 0,
          data: seller
        })
      })

      app.get('/api/goods', (req, res) => {
        // res.json(mockdata)
        res.json({
          errno: 0,
          data: goods
        })
      })

      app.get('/api/ratings', (req, res) => {
        // res.json(mockdata)
        res.json({
          errno: 0,
          data: ratings
        })
      })
    }
  }
}
